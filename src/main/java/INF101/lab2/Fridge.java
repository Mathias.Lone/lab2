package INF101.lab2;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;



public class Fridge implements IFridge{
    
    int max_size = 20;

    public int totalSize() {
        return max_size;

    }
    
    List <FridgeItem> fridgeItems = new ArrayList<FridgeItem>();

    

    @Override
    public int nItemsInFridge() {
        
        return fridgeItems.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridgeItems.size() < 20){
            fridgeItems.add(item);
            return true;
        }
        else{
            return false;
        }
        
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!fridgeItems.remove(item))throw new NoSuchElementException();
    
    }

    @Override
    public void emptyFridge() {
        fridgeItems.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List <FridgeItem> expiredFoods = new ArrayList<FridgeItem>();
        for (int i = 0; i < fridgeItems.size(); i++) {
            if (fridgeItems.get(i).hasExpired()) {
                expiredFoods.add(fridgeItems.get(i));
                
            }
        }
        fridgeItems.removeAll(expiredFoods);
        return expiredFoods;
    }
}



